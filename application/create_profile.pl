#!/usr/bin/perl
require '/web/cybertelusa.com/init.cgi';

my $acctid = $ARGV[0];
die "NO ACCTID\n\n" if !$acctid;


&load_cim_by_vm_acctid($acctid,'just_the_ids');

($web{'login_name'}) = split(/\#\#el\#\#/,(&vmdatabase('',"SELECT name FROM ifone..member WHERE acctid=$acctid"))[0]);
$body = '<profile>
	<merchantCustomerId>'.$logacctid.'</merchantCustomerId>
	<description>'.$web{'login_name'}.' ['.$logacctid.']</description>
	<email>'.$user_session_setup{'email'}.'</email>
	<paymentProfiles>
			<customerType>individual</customerType>
			<billTo>
					<firstName>'.$web{'fname'}.'</firstName>
					<lastName>'.$web{'lname'}.'</lastName>
					<address>'.$web{'address'}.''.$time.'</address>
					<city>'.$web{'city'}.'</city>
					<state>'.$web{'state'}.'</state>
					<zip>'.$web{'zip'}.'</zip>
					<country>'.$web{'country'}.'</country>
					<phoneNumber>'.$web{'phone'}.'</phoneNumber>
			</billTo>
			<payment>
			<creditCard>
							<cardNumber>'.$web{'cardnum'}.'</cardNumber>
							<expirationDate>'.$web{'year'}.'-'.$web{'month'}.'</expirationDate>
							<cardCode>'.$web{'cardverify'}.'</cardCode>
					</creditCard>
			</payment>
	</paymentProfiles>
</profile>
<validationMode>'.$config{'cim_validation'}.'</validationMode>';

warn "$body\n\n\n\n";
